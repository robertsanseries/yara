<?php

/*
|--------------------------------------------------------------------------
| Test PHP version
|--------------------------------------------------------------------------
|
| Check which version of PHP is running.
|
*/
$minPHPVersion = '7.1';
if (phpversion() < $minPHPVersion)
{
	die("Your PHP version must be {$minPHPVersion} or higher to run Yara. Current version: " . phpversion());
}

unset($minPHPVersion);


/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual.
|
*/
require __DIR__.'/../vendor/autoload.php';


/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser.
|
*/
$app = new \Yara\Yara();
$app->run();