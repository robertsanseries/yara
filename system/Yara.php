<?php

/*
| Yara
|
| An open source application development mini-framework for PHP
|
| This content is released under the MIT License (MIT)
| 
| Copyright (c) 2018 Robert San
| 
*/

namespace Yara;

class Yara
{
	
	public function __construct()
	{
		
	}

	public function run()
	{

	}

}